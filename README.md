# README #

Contains the code files from [http://www.newtonsoft.com/json](Link URL) with the code slightly refactored to run under unity. See the [commits log](https://bitbucket.org/createdbyx/newtonsoft.json-4-unity-project-files/commits/) for recent updates.

Supports Unity 5 both Free and Pro versions and includes converters for Color, Vector types, and Matrix4x4.

#![Logo](https://github.com/JamesNK/Newtonsoft.Json/raw/master/Doc/icons/logo.jpg) Json.NET#

- [Homepage](http://www.newtonsoft.com/json)
- [Documentation](http://www.newtonsoft.com/json/help)
- [NuGet Package](https://www.nuget.org/packages/Newtonsoft.Json)
- [Contributing Guidelines](https://github.com/JamesNK/Newtonsoft.Json/blob/master/CONTRIBUTING.md)
- [License](https://github.com/JamesNK/Newtonsoft.Json/blob/master/LICENSE.md)
- [Stack Overflow](http://stackoverflow.com/questions/tagged/json.net)